/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mukku.mbookbank;

/**
 *
 * @author Acer
 */
public class BookBank {
    String name;
    double balance;
    
    //Constructor : method
    BookBank(String name1, double balance1){
        name = name1;
        balance = balance1;
    }
    
    void deposit(double money){
        if(money <= 0){
            System.out.println("Money should more than 0!!");
            return;
        }
        balance += money;
    }
    
    void widthdraw(double money){
        if(money > balance){
            System.out.println("Not enough money for widthdraw!!");
            return;
        }
         balance -= money;
    }
}
